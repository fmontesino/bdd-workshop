package me.fmon.bddworkshop.app

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import me.fmon.bddworkshop.app.injection.DaggerAppComponent
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}