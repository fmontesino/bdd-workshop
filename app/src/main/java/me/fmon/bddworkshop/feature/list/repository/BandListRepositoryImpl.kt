package me.fmon.bddworkshop.feature.list.repository

import io.reactivex.Single
import me.fmon.bddworkshop.feature.list.model.Band
import me.fmon.bddworkshop.feature.list.network.BandListApi
import me.fmon.bddworkshop.feature.list.network.mapper.BandListMapper
import javax.inject.Inject

class BandListRepositoryImpl @Inject constructor(
    private val api: BandListApi,
    private val mapper: BandListMapper
) : BandListRepository {
    override fun loadBands(): Single<List<Band>> = api.getBands().map { mapper.fromResponse(it) }
}