package me.fmon.bddworkshop.base.injection

import dagger.Binds
import dagger.Module
import me.fmon.bddworkshop.base.network.SchedulerProvider
import me.fmon.bddworkshop.base.network.SchedulerProviderImpl
import javax.inject.Singleton

@Module
abstract class BaseModule {
    @Singleton
    @Binds
    abstract fun provideSchedulerProvider(schedulerProvider: SchedulerProviderImpl): SchedulerProvider
}