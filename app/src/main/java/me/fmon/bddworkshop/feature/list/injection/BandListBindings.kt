package me.fmon.bddworkshop.feature.list.injection

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.fmon.bddworkshop.app.injection.ActivityScope
import me.fmon.bddworkshop.app.navigation.AppIntentProvider
import me.fmon.bddworkshop.feature.list.mvp.BandListActivity
import me.fmon.bddworkshop.feature.list.mvp.BandListContract
import me.fmon.bddworkshop.feature.list.mvp.BandListPresenter
import me.fmon.bddworkshop.feature.list.navigation.BandListIntentProvider
import me.fmon.bddworkshop.feature.list.network.BandListApi
import me.fmon.bddworkshop.feature.list.network.StubbedBandListApi
import me.fmon.bddworkshop.feature.list.repository.BandListRepository
import me.fmon.bddworkshop.feature.list.repository.BandListRepositoryImpl

@Module
abstract class BandListBindings {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): BandListActivity

    @ActivityScope
    @Binds
    abstract fun provideMainPresenter(presenter: BandListPresenter): BandListContract.Presenter

    /**
     * Navigation
     */
    @ActivityScope
    @Binds
    abstract fun provideMainActivityIntentProvider(intentProvider: AppIntentProvider): BandListIntentProvider

    /**
     * Use Cases
     */

    // ╰(◕ヮ◕)つ¤=[]———

    /**
     * Repositories
     */
    @ActivityScope
    @Binds
    abstract fun provideRepository(repository: BandListRepositoryImpl): BandListRepository

    //Stubbing
    @Binds
    abstract fun provideApi(api: StubbedBandListApi): BandListApi
}


/**
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓▓╪░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╪▓╪▓░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓╪╪▓░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╪▓╪╪▓╪
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╪╪╪╪╪╪
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓╪╪╪╪╪
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓╪╪╪╪╪
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╪▓╪╪╪╪╪
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╪▓╪╪╪╪╪
░░░░░░░░░░░░░░░░░░╪░░░░░░░░░░░░░░▓╪╪╪╪╪╪
░░░░░░░░░░░░░░░░░╪██▓╪╪░░░░░░░░░░▓╪╪╪╪▓╪
░░░░░░░░░░░░░░░░░███████▓░░░░░░░░▓╪╪╪╪▓╪
░░░░░░░░░░░░░░░░░█████████╪░░░░░▓▓╪╪╪╪▓░
░╪╪░░░░░░░░░░░░░▓████████╪▓╪░░░░█▓╪╪╪╪▓░
░▓▓╪░░░░░░░░░░░▓█████████░╪▓░░░░█╪╪╪╪╪▓░
░▓╪▓╪░░░░░░░░░░█████████▓▓██╪░░▓█╪╪╪╪▓╪░
░▓╪╪▓╪░░░░░░░░██████████╪███╪░░██╪╪╪▓▓╪░
░▓╪╪╪▓░░░░░░░▓██████████╪███▓░╪██╪╪▓╪░░░
░▓╪╪╪▓╪░░░░░░█╪█████████╪███▓╪███▓╪╪▓░░░
░╪▓╪╪╪▓╪░░░░╪▓░╪████████▓███▓█████╪╪▓░░░
░░╪▓╪╪▓▓░░░░╪▓█╪▓███████▓██▓██████╪▓╪░░░
░░▓╪╪╪╪▓╪░░░╪███╪████████╪╪▓████████░░░░
░╪▓╪╪╪╪╪▓░░░░████▓█████████████████░░░░░
░░▓╪╪╪╪╪▓▓░░░████▓███████████████▓░░░░░░
░░▓╪╪╪╪╪╪██░░▓███▓████████████╪╪░░░░░░░░
░░╪▓╪╪╪╪╪▓██╪▓██▓▓████▓█░███▓╪░░░░░░░░░░
░░░▓╪╪╪╪╪╪████╪░▓███▓███████╪░░░░░░░░░░░
░░░╪▓╪╪╪╪▓█████████▓███▓╪█▓╪░░░░░░░░░░░░
░░░░▓╪╪╪╪█████████▓░╪▓╪░░╪▓░░░░░░░░░░░░░
░░░░╪▓╪╪╪█████████░╪░░╪╪╪▓▓░░░░░░░░░░░░░
░░░░░╪▓╪╪██████▓▓╪░╪╪░╪╪▓██░░░░░░░░░░░░░
░░░░░░╪▓████████▓╪╪▓▓▓▓▓▓██▓░░░░░░░░░░░░
░░░░░░░╪▓█▓▓╪░░╪╪▓██▓▓▓╪▓███╪░░░░░░░░░░░
░░░░░░░░░░░░░░░░░███╪▓╪╪▓████╪░░░░░░░░░░
░░░░░░░░░░░░░░░░████╪▓▓╪▓█████▓░░░░░░░░░
░░░░░░░░░░░░░░░▓████╪▓▓╪╪██████╪░░░░░░░░
░░░░░░░░░░░░░░██████╪▓▓╪▓██████▓░░░░░░░░
░░░░░░░░░░░░░██████▓╪▓▓▓████████░░░░░░░░
░░░░░░░░░░░░░███████╪╪╪████████▓░░░░░░░░
░░░░░░░░░░░░╪████████╪╪████████▓░░░░░░░░
░░░░░░░░░░░░╪████████╪╪████████░░░░░░░░░
░░░░░░░░░░░░░████████╪▓███████▓░░░░░░░░░
░░░░░░░░░░░░░▓███████▓▓██████▓░░░░░░░░░░
░░░░░░░░░░░░░░████████▓███▓█░░░░░░░░░░░░
░░░░░░░░░░░░░░╪███▓███╪███╪█░░░░░░░░░░░░
░░░░░░░░░░░░░░░░▓█╪███╪╪██╪█╪░░░░░░░░░░░
░░░░░░░░░░░░░░░░▓█╪██╪╪╪╪╪╪██░░░░░░░░░░░
░░░░░░░░░░░░░░░░██╪█╪╪╪▓╪╪╪██▓░░░░░░░░░░
░░░░░░░░░░░░░░░▓██╪╪╪▓▓▓▓╪╪███╪░░░░░░░░░
░░░░░░░░░░░░░░╪███╪╪╪▓▓▓╪╪▓████░░░░░░░░░
░░░░░░░░░░░░░░████▓╪▓▓▓▓▓╪▓████╪░░░░░░░░
░░░░░░░░░░░░░▓█████╪╪▓▓▓╪╪██████░░░░░░░░
░░░░░░░░░░░░░██████▓╪╪╪╪╪▓██████╪░░░░░░░
░░░░░░░░░░░░╪███████▓╪╪╪▓███████▓░░░░░░░
░░░░░░░░░░░░▓███████████████████▓░░░░░░░
░░░░░░░░░░░░╪██████▓╪███╪╪███████░░░░░░░
░░░░░░░░░░░░██████▓░░░░░░░░██████▓░░░░░░
░░░░░░░░░░░░██████▓░░░░░░░░██████╪░░░░░░
░░░░░░░░░░░░██████▓░░░░░░░░█████▓░░░░░░░
░░░░░░░░░░░░░█████░░░░░░░░░░██╪▓░░░░░░░░
**/