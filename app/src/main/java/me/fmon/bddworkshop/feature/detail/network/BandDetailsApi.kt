package me.fmon.bddworkshop.feature.detail.network

import io.reactivex.Single
import me.fmon.bddworkshop.feature.detail.network.response.BandDetailsResponse

interface BandDetailsApi {
    fun getBandDetails(name: String): Single<BandDetailsResponse>
}