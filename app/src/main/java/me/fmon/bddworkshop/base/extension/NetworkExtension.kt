package me.fmon.bddworkshop.base.extension

import java.io.IOException

fun Throwable.isNetworkError() = this is IOException