package me.fmon.bddworkshop.base.mvp

import android.os.Bundle
import androidx.annotation.CallSuper

open class BasePresenter<V : BaseContract.View> : BaseContract.Presenter<V> {

    override var view: V? = null

    @CallSuper
    override fun onCreate() {
        // Do nothing
    }

    @CallSuper
    override fun onStart() {
        // Do nothing
    }

    @CallSuper
    override fun onStop() {
        // Do nothing
    }

    @CallSuper
    override fun onDestroy() {
        // Do nothing
    }

    @CallSuper
    override fun onPause() {
        // Do nothing
    }

    @CallSuper
    override fun onResume() {
        // Do nothing
    }

    @CallSuper
    override fun onSaveState(bundle: Bundle): Bundle {
        return bundle
    }

    @CallSuper
    override fun onRestoreState(bundle: Bundle?) {
        //Do nothing
    }
}