package me.fmon.bddworkshop.feature.list.network.response

import com.google.gson.annotations.SerializedName

data class BandListResponse(
    @SerializedName("bands") val bands: List<Band>? = null
) {
    data class Band(
        @SerializedName("bandName") val name: String? = null,
        @SerializedName("bandGenre") val genre: String? = null,
        @SerializedName("bandYear") val year: Int? = null,
        @SerializedName("bandCountry") val country: String? = null
    )
}