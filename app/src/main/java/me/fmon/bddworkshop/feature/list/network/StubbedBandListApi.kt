package me.fmon.bddworkshop.feature.list.network

import io.reactivex.Single
import me.fmon._stubbing.Stubbing
import me.fmon.bddworkshop.feature.list.network.response.BandListResponse
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class StubbedBandListApi @Inject constructor() : BandListApi {
    override fun getBands(): Single<BandListResponse> =
        Single.just(BandListResponse(Stubbing.bands)).delay(5, TimeUnit.SECONDS)
}

/**

      _ _,---._
   ,-','       `-.___
  /-;'               `._
 /\/          ._   _,'o \
( /\       _,--'\,','"`. )
 |\      ,'o     \'    //\
 |      \        /   ,--'""`-.
 :       \_    _/ ,-'         `-._
  \        `--'  /                )            _   _
   `.  \`._    ,'     ________,','            | \ | |
     .--`     ,'  ,--` __\___,;'              |  \| | ___ _ __ ___  __ _
      \`.,-- ,' ,`_)--'  /`.,'                | . ` |/ _ \ '__/ _ \/ _` |
       \( ;  | | )      (`-/                  | |\  |  __/ | |  __/ (_| |
         `--'| |)       |-/                   \_| \_/\___|_|  \___|\__,_|
           | | |        | |
           | | |,.,-.   | |_
           | `./ /   )---`  )
          _|  /    ,',   ,-'
         ,'|_(    /-<._,' |--,
         |    `--'---.     \/ \
         |          / \    /\  \
       ,-^---._     |  \  /  \  \
    ,-'        \----'   \/    \--`.
   /            \              \   \

 **/