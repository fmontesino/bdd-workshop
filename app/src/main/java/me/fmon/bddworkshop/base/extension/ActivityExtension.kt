package me.fmon.bddworkshop.base.extension

import android.app.Activity

inline fun <reified T : Any> Activity.intentExtra(key: String): Lazy<T> = lazy { intent?.extras?.get(key) as T }