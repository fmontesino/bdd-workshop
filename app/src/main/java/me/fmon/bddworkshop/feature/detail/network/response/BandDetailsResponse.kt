package me.fmon.bddworkshop.feature.detail.network.response

import com.google.gson.annotations.SerializedName

data class BandDetailsResponse(
    @SerializedName("band_name") val name: String? = null,
    @SerializedName("band_country") val country: String? = null,
    @SerializedName("band_genre") val genre: String? = null,
    @SerializedName("band_year") val year: Int? = null,
    @SerializedName("band_website") val website: String? = null,
    @SerializedName("band_albums") val albums: List<Album>? = null
) {
    data class Album(
        @SerializedName("album_name") val name: String? = null,
        @SerializedName("album_year") val year: Int? = null
    )
}

