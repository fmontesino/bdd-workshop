package me.fmon.bddworkshop.feature.list.mvp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import me.fmon.bddworkshop.R
import me.fmon.bddworkshop.base.mvp.BaseActivity
import me.fmon.bddworkshop.feature.list.model.Band
import me.fmon.bddworkshop.feature.list.mvp.BandListContract.*
import me.fmon.bddworkshop.feature.list.navigation.BandListIntentProvider
import timber.log.Timber
import javax.inject.Inject

class BandListActivity : BaseActivity<View, Presenter>(), View, Navigation {

    companion object {
        fun getIntent(context: Context) = Intent(context, BandListActivity::class.java)
    }

    @Inject
    override lateinit var presenter: Presenter

    @Inject
    lateinit var intentProvider: BandListIntentProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        presenter.navigation = this
    }

    override fun onDestroy() {
        presenter.navigation = null
        super.onDestroy()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    override fun restartApplication() {
        Timber.e("restartApplication()")
        startActivity(intentProvider.getResetAppIntent(this))
        finish()
    }

    override fun goToBandDetails(name: String) {
        Timber.e("goToBandDetails($name)")
        startActivity(intentProvider.getBandDetailsIntent(this, name))
    }

    override fun showLoading() {
        Timber.e("showLoading()")
    }

    override fun hideLoading() {
        Timber.e("hideLoading()")
    }

    override fun showBandList(bands: List<Band>) {
        Timber.e("showBandList(): $bands")
    }

    override fun showNetworkError() {
        Timber.e("showNetworkError()")
    }

    override fun showUnknownError() {
        Timber.e("showUnknownError()")
    }
}
