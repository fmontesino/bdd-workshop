package me.fmon.bddworkshop.base.domain

interface UseCase<P, C> {

    var callback: C?

    fun execute(params: P)

    fun cancel()
}