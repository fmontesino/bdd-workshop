package me.fmon.bddworkshop.base.network

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

interface SchedulerProvider {

    val mainScheduler: Scheduler

    val ioScheduler: Scheduler
}

class SchedulerProviderImpl @Inject constructor() : SchedulerProvider {
    override val mainScheduler: Scheduler = AndroidSchedulers.mainThread()
    override val ioScheduler: Scheduler = Schedulers.io()
}