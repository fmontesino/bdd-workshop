package me.fmon.bddworkshop

import io.reactivex.schedulers.Schedulers
import me.fmon.bddworkshop.base.network.SchedulerProvider

class TestSchedulerProvider : SchedulerProvider {

    override val mainScheduler = Schedulers.trampoline()

    override val ioScheduler = Schedulers.trampoline()
}