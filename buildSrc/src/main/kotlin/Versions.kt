import kotlin.String

/**
 * Find which updates are available by running
 *     `$ ./gradlew buildSrcVersions`
 * This will only update the comments.
 *
 * YOU are responsible for updating manually the dependency version. */
object Versions {
    const val appcompat: String = "1.1.0-alpha02" 

    const val constraintlayout: String = "2.0.0-alpha3" 

    const val espresso_core: String = "3.1.2-alpha01" 

    const val androidx_test_runner: String = "1.1.2-alpha01" 

    const val aapt2: String = "3.2.1-4818971" // available: "3.3.1-5013011"

    const val com_android_tools_build_gradle: String = "3.2.1" // available: "3.3.1"

    const val lint_gradle: String = "26.2.1" // available: "26.3.1"

    const val gson: String = "2.8.5" 

    const val com_google_dagger: String = "2.16" 

    const val timber: String = "4.7.1" 

    const val com_squareup_retrofit2: String = "2.5.0" 

    const val de_fayard_buildsrcversions_gradle_plugin: String = "0.3.2" 

    const val rxandroid: String = "2.1.0"

    const val rxjava: String = "2.1.14" // available: "2.2.6"

    const val junit: String = "4.12" 

    const val assertj_core: String = "3.11.1"

    const val org_jetbrains_kotlin: String = "1.3.10" // available: "1.3.21"

    const val mockito_inline: String = "2.24.0"

    /**
     *
     *   To update Gradle, edit the wrapper file at path:
     *      ./gradle/wrapper/gradle-wrapper.properties
     */
    object Gradle {
        const val runningVersion: String = "4.6"

        const val currentVersion: String = "5.2.1"

        const val nightlyVersion: String = "5.3-20190209000057+0000"

        const val releaseCandidate: String = ""
    }
}
