package me.fmon.bddworkshop.feature.list.network

import io.reactivex.Single
import me.fmon.bddworkshop.feature.list.network.response.BandListResponse
import java.lang.Exception

interface BandListApi {
    @Throws(Exception::class)
    fun getBands(): Single<BandListResponse>
}