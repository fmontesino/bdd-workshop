package me.fmon.bddworkshop.base.mvp

import android.os.Bundle

interface BaseContract {

    interface View

    interface Presenter<V : View> {

        var view: V?

        fun onCreate()

        fun onStart()

        fun onStop()

        fun onDestroy()

        fun onPause()

        fun onResume()

        fun onSaveState(bundle: Bundle): Bundle

        fun onRestoreState(bundle: Bundle?)
    }
}