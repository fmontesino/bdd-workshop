package me.fmon.bddworkshop.feature.list.repository

import io.reactivex.Single
import me.fmon.bddworkshop.feature.list.model.Band

interface BandListRepository {
    fun loadBands(): Single<List<Band>>
}

