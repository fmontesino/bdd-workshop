package me.fmon.bddworkshop.app.navigation

import android.content.Context
import me.fmon.bddworkshop.base.navigation.BaseIntentProvider
import me.fmon.bddworkshop.feature.detail.mvp.BandDetailActivity
import me.fmon.bddworkshop.feature.list.mvp.BandListActivity
import me.fmon.bddworkshop.feature.list.navigation.BandListIntentProvider
import javax.inject.Inject

class AppIntentProvider @Inject constructor() : BaseIntentProvider, BandListIntentProvider {
    override fun getResetAppIntent(context: Context) = BandListActivity.getIntent(context)

    override fun getBandDetailsIntent(context: Context, name: String) = BandDetailActivity.getIntent(context, name)
}