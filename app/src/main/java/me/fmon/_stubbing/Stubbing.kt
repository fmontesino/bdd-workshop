package me.fmon._stubbing

import me.fmon.bddworkshop.feature.detail.model.Album
import me.fmon.bddworkshop.feature.detail.model.Band
import java.security.InvalidParameterException
import me.fmon.bddworkshop.feature.detail.network.response.BandDetailsResponse as ApiDetailsBand
import me.fmon.bddworkshop.feature.detail.network.response.BandDetailsResponse.Album as ApiDetailsAlbum
import me.fmon.bddworkshop.feature.list.network.response.BandListResponse.Band as ApiListBand


/**

                            /  \
                           |    |
             _             |    |
           /' |            | _  |
          |   |            |    |
          | _ |            |    |
          |   |            |    |
          |   |        __  | _  |
          | _ |  __   /  \ |    |
          |   | /  \ |    ||    |
          |   ||    ||    ||    |       _---.
          |   ||    ||    |. __ |     ./     |
          | _. | -- || -- |    `|    /      //
          |'   |    ||    |     |   /`     (/
          |    |    ||    |     | ./       /
          |    |.--.||.--.|  __ |/       .|
          |  __|    ||    |-'            /
          |-'   \__/  \__/             .|
          |       _.-'                 /
          |   _.-'      /             |
          |            /             /
          |           |             /
          `           |            /
           \          |          /'
            |          `        /
             \                .'
             |                |
             |                |
             |                |
             |                |


**/


object Stubbing {

    val bands: List<ApiListBand>
        get() = listOf(_slipknot, _volbeat, _parkway, _steel)

    @Throws(InvalidParameterException::class)
    fun detailsFor(name: String) =
        when (name) {
            slipknot.name -> slipknot.toResponse()
            volbeat.name -> volbeat.toResponse()
            parkway.name -> parkway.toResponse()
            steel.name -> steel.toResponse()
            else -> throw InvalidParameterException("Band $name not found")
        }


    private val slipknot1 = Album(name = "Slipknot", year = 1999)
    private val slipknot2 = Album(name = "Iowa", year = 2001)
    private val slipknot3 = Album(name = "Vol 3: (The Subliminal Verses", year = 2004)
    private val slipknot4 = Album(name = "All Hope Is Gone", year = 2008)
    private val slipknot5 = Album(name = ".5: The Gray Chapter", year = 2014)

    // ( ˘ ³˘)♥
    private val slipknot =
        Band(
            name = "Slipknot",
            country = "Iowa, USA",
            genre = "Nu-Metal",
            year = 1995,
            website = "www.slipknot1.com",
            albums = listOf(slipknot1, slipknot2, slipknot3, slipknot4, slipknot5)
        )

    private val _slipknot =
        ApiListBand(
            name = slipknot.name,
            year = slipknot.year,
            genre = null,
            country = null
        )

    private val volbeat1 = Album(name = "The Strength / The Sound / The Songs", year = 2005)
    private val volbeat2 = Album(name = "Rock the Rebel / Metal the Devil", year = 2007)
    private val volbeat3 = Album(name = "Guitar Gangsters & Cadillac Blood", year = 2008)
    private val volbeat4 = Album(name = "Beyond Hell / Above Heaven", year = 2010)
    private val volbeat5 = Album(name = "Outlaw Gentlemen & Shady Ladies", year = 2013)
    private val volbeat6 = Album(name = "Seal the Deal & Let's Boogie", year = 2016)

    private val volbeat =
        Band(
            name = "Volbeat",
            country = "Copenhagen, Denmark",
            genre = "Elvis Metal",
            year = 2001,
            website = "www.volbeat.dk",
            albums = listOf(volbeat1, volbeat2, volbeat3, volbeat4, volbeat5, volbeat6)

        )

    private val _volbeat =
        ApiListBand(
            name = volbeat.name,
            country = volbeat.country,
            genre = volbeat.genre,
            year = volbeat.year
        )

    private val pwd1 = Album(name = "Killing with a Smile", year = 2005)
    private val pwd2 = Album(name = "Horizons", year = 2007)
    private val pwd3 = Album(name = "Deep Blue", year = 2010)
    private val pwd4 = Album(name = "Atlas", year = 2012)
    private val pwd5 = Album(name = "Ire", year = 2015)
    private val pwd6 = Album(name = "Reverence", year = 2018)

    private val parkway =
        Band(
            name = "Parkway Drive",
            country = "Byron Bay, Australia",
            genre = "Metalcore",
            year = 2003,
            website = "www.parkwaydriverock.com",
            albums = listOf(pwd1, pwd2, pwd3, pwd4, pwd5, pwd6)
        )

    private val _parkway =
        ApiListBand(
            name = parkway.name,
            country = parkway.country,
            genre = null,
            year = parkway.year
        )

    private val steel1 = Album(name = "Feel the Steel", year = 2009)
    private val steel2 = Album(name = "Balls Out", year = 2011)
    private val steel3 = Album(name = "All You Can Eat", year = 2014)
    private val steel4 = Album(name = "Lower the Bar", year = 2017)

    private val steel =
        Band(
            name = "Steel Panther",
            country = "Los Angeles, USA",
            genre = "Glam Rock",
            year = 2000,
            website = "www.steelpantherrocks.com",
            albums = listOf(steel1, steel2, steel3, steel4)
        )

    private val _steel =
        ApiListBand(
            name = steel.name,
            country = steel.country,
            genre = steel.genre,
            year = parkway.year
        )

    private fun Band.toResponse() = ApiDetailsBand(
        name = this.name,
        country = this.country,
        genre = this.genre,
        year = this.year,
        website = this.website,
        albums = this.albums.map { ApiDetailsAlbum(it.name, it.year) }
    )
}