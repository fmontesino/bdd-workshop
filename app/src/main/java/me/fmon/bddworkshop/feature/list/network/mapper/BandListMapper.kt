package me.fmon.bddworkshop.feature.list.network.mapper

import me.fmon.bddworkshop.feature.list.model.Band
import me.fmon.bddworkshop.feature.list.network.response.BandListResponse
import java.io.IOException
import javax.inject.Inject

//TODO: Add Unit Tests for this class
class BandListMapper @Inject constructor() {

    companion object {
        private const val DEFAULT_VALUE = "-"
    }

    @Throws(IOException::class)
    fun fromResponse(response: BandListResponse): List<Band> {
        val bands = arrayListOf<Band>()
        response.bands?.forEach {
            bands.add(
                Band(
                    name = it.name.mapOrDefault(),
                    genre = it.genre.mapOrDefault(),
                    year = it.year ?: throw IOException("Failed to map required field: year") // ಡ_ಡ
                )
            )
        }
        return bands
    }

    private fun String?.mapOrDefault(default: String = DEFAULT_VALUE): String {
        if (this == null.toString() || this.isNullOrEmpty()) return default
        return this
    }
}