package me.fmon.bddworkshop.feature.detail.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Band(
    val name: String,
    val country: String,
    val genre: String,
    val year: Int,
    val website: String,
    val albums: List<Album>
) : Parcelable