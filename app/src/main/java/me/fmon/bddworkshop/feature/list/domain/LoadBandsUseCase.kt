package me.fmon.bddworkshop.feature.list.domain

import me.fmon.bddworkshop.base.domain.UseCase
import me.fmon.bddworkshop.feature.list.model.Band

interface LoadBandsUseCase : UseCase<LoadBandsUseCase.Params, LoadBandsUseCase.Callback> {

    override fun execute(params: LoadBandsUseCase.Params)

    object Params  // (╯°□°）╯︵ ┻━┻

    sealed class Result {
        data class Success(val bands: List<Band>) : Result()
        object NetworkError : Result()
        object UnknownError : Result()
    }

    interface Callback {
        fun onLoadBandsResult(result: Result)
    }
}