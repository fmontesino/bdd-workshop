package me.fmon.bddworkshop.feature.detail.network

import io.reactivex.Single
import me.fmon._stubbing.Stubbing
import me.fmon.bddworkshop.feature.detail.network.response.BandDetailsResponse
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class StubbedBandDetailsApi @Inject constructor() : BandDetailsApi {
    override fun getBandDetails(name: String): Single<BandDetailsResponse> =
        Single.just(Stubbing.detailsFor(name)).delay(5, TimeUnit.SECONDS)
}