package me.fmon.bddworkshop.feature.list.mvp

import me.fmon.bddworkshop.base.mvp.BaseContract
import me.fmon.bddworkshop.feature.list.model.Band

interface BandListContract {
    interface Navigation {
        fun restartApplication()

        fun goToBandDetails(name: String)
    }

    interface View : BaseContract.View {
        fun showLoading()

        fun hideLoading()

        fun showBandList(bands: List<Band>)

        fun showNetworkError()

        fun showUnknownError()
    }

    interface Presenter : BaseContract.Presenter<View> {
        var navigation: Navigation?

        fun loadBands()

        fun onBackPressed()
    }
}