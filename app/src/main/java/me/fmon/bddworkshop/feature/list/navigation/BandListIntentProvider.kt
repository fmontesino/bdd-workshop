package me.fmon.bddworkshop.feature.list.navigation

import android.content.Context
import android.content.Intent

interface BandListIntentProvider {
    fun getResetAppIntent(context: Context): Intent

    fun getBandDetailsIntent(context: Context, name: String): Intent
}