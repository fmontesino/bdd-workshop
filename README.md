## BDD Workshop (ǝɟᴉן ʎɯ ɟo ǝʌoן ǝɥʇ ɹoɟ)

This workshop is intended to showcase some of the most common patterns used in Android development:

- MVP pattern
- Clean Architecture
- Dagger
- UseCases & Repositories
- Data Models & Mappers
- Navigation with IntentProviders
- Unit Test
- Behavior Driven Development
_____________________

### Band List scenarios

#### Scenario 1 - Show Bands List Loading State

**Given** I am on the main screen

**When** the API call is still loading

**Then** I see a "Loading" message in the screen

#### Scenario 2 - Show Bands List happy path

**Given** I am on the Loading state 

**When** I get a successful response from the API

**Then** the Loading message is hidden

**And** I see a list of bands

#### Scenario 3 - Show Bands List missing information
**Given** I get a successful response from the API 

**When** some of the band data is missing

**Then** I see a dash (-) in the corresponding field

#### Scenario 4 - Show Bands List network error

**Given** I get a network error from the API 

**When** I am on the main screen

**Then** I see a fullscreen error message

#### Scenario 5 - Show Bands List unknown error

**Given** I am on the Loading state

**When** I get a network error from the API

**Then** the Loading message is hidden 

**And** I see a snackbar message with the option to try again

#### Scenario 6 - Back button behavior

**Given** I am on the main screen 

**When** I tap the hardware back button

**Then** the app launches again

#### Scenario 7 - Navigate to Band Details

**Given** I see a list of bands 

**When** I click on a band in the list

**Then** I navigate to the details screen for that band

_____________________

### Band Details scenarios

#### Scenario 1 - Show Band Details Loading state

**Given** I am on the details screen 

**When** the API call is still loading

**Then** I see a "Loading" message in the screen


#### Scenario 2 - Show Band Details happy path

**Given** I am in the Loading state 

**When** I get a successful response from the API

**Then** the "Loading" message is hidden

**And** I see all the information about the band (name, year, genre, country, website and album list)

#### Scenario 2 - Show Band Details missing information

**Given** I get a successful response from the API 

**When** some of the band data is missing

**Then** I see a dash (-) in the corresponding field

#### Scenario 2 - Show Band Details error handling

**Given** I am on the Loading state

**When** I get a network or unknown error from the API 

**Then** I navigate back to the band list screen

**And** I see a toast with the text "Oops, something went wrong"

#### Scenario 3 - Back button behavior

**Given** I am on the details screen

**When** I tap the hardware back button

**Then** I navigate back to the band list screen

**And** the list reloads

#### Scenario 4 - Click on website

**Given** I am on the details screen

**When** I tap the artist's website

**Then** a browser opens a new tab to the website

