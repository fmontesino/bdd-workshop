package me.fmon.bddworkshop.feature.list.repository

import io.reactivex.Single
import me.fmon.bddworkshop.feature.list.model.Band
import me.fmon.bddworkshop.feature.list.network.BandListApi
import me.fmon.bddworkshop.feature.list.network.mapper.BandListMapper
import me.fmon.bddworkshop.feature.list.network.response.BandListResponse
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BandListRepositoryImplTest {

    companion object {
        private val BAND_LIST = listOf(mock(Band::class.java))
    }

    @Mock
    private lateinit var mockApi: BandListApi

    @Mock
    private lateinit var mockMapper: BandListMapper

    @Mock
    private lateinit var mockResponse: BandListResponse

    @InjectMocks
    private lateinit var subject: BandListRepositoryImpl

    @Test
    fun loadBands() {
        given(mockMapper.fromResponse(mockResponse)).willReturn(BAND_LIST)
        given(mockApi.getBands()).willReturn(Single.just(mockResponse))

        val testObserver = subject.loadBands().test()

        testObserver.assertValue(BAND_LIST)
    }
}