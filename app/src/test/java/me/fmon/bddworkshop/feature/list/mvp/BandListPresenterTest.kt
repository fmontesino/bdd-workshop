package me.fmon.bddworkshop.feature.list.mvp

import me.fmon.bddworkshop.feature.list.domain.LoadBandsUseCase
import me.fmon.bddworkshop.feature.list.model.Band
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.inOrder
import org.mockito.junit.MockitoJUnitRunner

//TODO: Task 3: I am broken, fix me
@RunWith(MockitoJUnitRunner::class)
class BandListPresenterTest {

    @Mock
    private lateinit var mockUseCase: LoadBandsUseCase

    @Mock
    private lateinit var mockNavigation: BandListContract.Navigation

    @Mock
    private lateinit var mockView: BandListContract.View

    @Mock
    private lateinit var mockSuccessResult: LoadBandsUseCase.Result.Success

    @Mock
    private lateinit var mockBand: Band

    @InjectMocks
    private lateinit var subject: BandListPresenter

    @Before
    fun setup() {
        subject.apply {
            this.navigation = mockNavigation
            this.view = mockView
        }
    }

    @Test
    fun `onBackPressed() - navigation restarts the app`() {
        subject.onBackPressed()

        then(mockNavigation).should().restartApplication()
    }

    @Test
    fun `onDestroy() - usecase is cancelled`() {
        subject.onDestroy()

        then(mockUseCase).should().cancel()
    }

    @Test
    fun `onLoadBands() - view loads and usecase is executed`() {
        subject.loadBands()

        val inOrder = inOrder(mockView, mockUseCase)

        then(mockUseCase).should(inOrder).execute(LoadBandsUseCase.Params)
        then(mockView).should(inOrder).showLoading()
    }

    @Test
    fun `onLoadBandsResult() - happy path`() {
        val bands = listOf(mockBand)
        given(mockSuccessResult.bands).willReturn(bands)

        subject.onLoadBandsResult(mockSuccessResult)

        val inOrder = inOrder(mockView)

        then(mockView).apply {
            should(inOrder).hideLoading()
            should(inOrder).showBandList(bands)
            shouldHaveNoMoreInteractions()
        }
    }

    @Test
    fun `onLoadBandsResult() - network error`() {
        subject.onLoadBandsResult(LoadBandsUseCase.Result.NetworkError)

        val inOrder = inOrder(mockView)

        then(mockView).apply {
            should(inOrder).hideLoading()
            should(inOrder).showNetworkError()
            shouldHaveNoMoreInteractions()
        }
    }

    @Test
    fun `onLoadBandsResult() - unknown error`() {
        subject.onLoadBandsResult(LoadBandsUseCase.Result.UnknownError)

        val inOrder = inOrder(mockView)

        then(mockView).apply {
            should(inOrder).hideLoading()
            should(inOrder).showUnknownError()
            shouldHaveNoMoreInteractions()
        }
    }
}

/**

88888888ba                                           88
88      "8b                         ,d               88
88      ,8P                         88               88
88aaaaaa8P' ,adPPYYba, 8b,dPPYba, MM88MMM ,adPPYba,  88  ,adPPYba,
88""""""8b, ""     `Y8 88P'   "Y8   88   a8"     "8a 88 a8"     "8a
88      `8b ,adPPPPP88 88           88   8b       d8 88 8b       d8
88      a8P 88,    ,88 88           88,  "8a,   ,a8" 88 "8a,   ,a8"
88888888P"  `"8bbdP"Y8 88           "Y888 `"YbbdP"'  88  `"YbbdP"'



 ,adPPYba, ,adPPYba,
a8P_____88 I8[    ""
8PP"""""""  `"Y8ba,
"8b,   ,aa aa    ]8I
 `"Ybbd8"' `"YbbdP"'


                      88                                   88
88                    88                                88 88
                      88                                   88
88 88,dPYba,,adPYba,  88,dPPYba,   ,adPPYba,  ,adPPYba, 88 88
88 88P'   "88"    "8a 88P'    "8a a8P_____88 a8"     "" 88 88
88 88      88      88 88       d8 8PP""""""" 8b         88 88
88 88      88      88 88b,   ,a8" "8b,   ,aa "8a,   ,aa 88 88
88 88      88      88 8Y"Ybbd8"'   `"Ybbd8"'  `"Ybbd8"' 88 88

 **/