package me.fmon.bddworkshop.feature.detail.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Album(
    val name: String,
    val year: Int
) : Parcelable