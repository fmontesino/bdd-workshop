package me.fmon.bddworkshop.base.navigation

import android.content.Context
import android.content.Intent

interface BaseIntentProvider {
    fun getResetAppIntent(context: Context): Intent
}