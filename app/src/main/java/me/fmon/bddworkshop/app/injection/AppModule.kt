package me.fmon.bddworkshop.app.injection

import dagger.Module
import dagger.Provides
import me.fmon.bddworkshop.app.navigation.AppIntentProvider
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    internal fun provideActivityIntentProvider() = AppIntentProvider()
}