package me.fmon.bddworkshop.feature.detail.mvp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import me.fmon.bddworkshop.R
import me.fmon.bddworkshop.base.extension.bindView
import me.fmon.bddworkshop.base.extension.intentExtra

class BandDetailActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_NAME = "extra_name"

        fun getIntent(context: Context, name: String) =
            Intent(context, BandDetailActivity::class.java).apply {
                putExtra(EXTRA_NAME, name)
            }
    }

    private val name by intentExtra<String>(EXTRA_NAME)

    private val textView by bindView<TextView>(R.id.text_view)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        textView.text = name
    }
}
