package me.fmon.bddworkshop.feature.detail.repository

import io.reactivex.Single
import me.fmon.bddworkshop.feature.detail.model.Band

interface BandDetailsRepository {
    fun getBandDetails(name: String): Single<Band>
}