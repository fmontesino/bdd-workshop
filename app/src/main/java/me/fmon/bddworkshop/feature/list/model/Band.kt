package me.fmon.bddworkshop.feature.list.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Band(
    val name: String,
    val genre: String,
    val year: Int
) : Parcelable