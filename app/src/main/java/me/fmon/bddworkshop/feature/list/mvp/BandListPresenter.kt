package me.fmon.bddworkshop.feature.list.mvp

import me.fmon.bddworkshop.base.mvp.BasePresenter
import me.fmon.bddworkshop.feature.list.domain.LoadBandsUseCase
import me.fmon.bddworkshop.feature.list.model.Band
import javax.inject.Inject

//TODO: Task 2: Uncomment this
class BandListPresenter @Inject constructor(
//    private val useCaseLoad: LoadBandsUseCase
) : BasePresenter<BandListContract.View>(), BandListContract.Presenter, LoadBandsUseCase.Callback {

    init {
//        useCaseLoad.callback = this
    }

    override var navigation: BandListContract.Navigation? = null

    override fun onBackPressed() {
        navigation?.restartApplication()
    }

    override fun onDestroy() {
//        useCaseLoad.cancel()
        super.onDestroy()
    }

    override fun loadBands() {
        view?.showLoading()
//        useCaseLoad.execute(LoadBandsUseCase.Params)
    }

    override fun onLoadBandsResult(result: LoadBandsUseCase.Result) {
        when(result){
            is LoadBandsUseCase.Result.Success -> onBandsLoaded(result.bands)
            is LoadBandsUseCase.Result.NetworkError -> onNetworkError()
            is LoadBandsUseCase.Result.UnknownError -> onError()
        }
    }

    private fun onBandsLoaded(bands: List<Band>) {
        view?.apply {
            hideLoading()
            showBandList(bands)
        }
    }

    private fun onError() {
        view?.apply {
            hideLoading()
            showNetworkError()
        }
    }

    private fun onNetworkError() {
        view?.apply {
            hideLoading()
            showNetworkError()
        }
    }
}